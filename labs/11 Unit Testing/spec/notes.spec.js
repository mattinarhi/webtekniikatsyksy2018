describe('notes module', function () {
	beforeEach(function () {
		notes.clear();
		notes.add('first note');
		notes.add('second note');
		notes.add('third note');
		notes.add('fourth note');
		notes.add('fifth note');

	});
	it('should be able to add a new note', function () {
		expect(notes.add('sixth note')).toBe(true);
		expect(notes.count()).toBe(6);
	});
	it('should ignore blank notes', function () {
		expect(notes.add('')).toBe(false);
		expect(notes.count()).toBe(5);
	});
	it('should ignore notes containing only whitespaces', function () {
		expect(notes.add(' ')).toBe(false);
		expect(notes.count()).toBe(5);
		//	pending();
	});
	it('should require a string parameter', function () {
		expect(notes.add()).toBe(false);
		expect(notes.count()).toBe(5);
		//	pending();
	});


});
describe('deleting a note', function() {
	beforeEach(function () {
		notes.clear();
		notes.add('first note');
		notes.add('second note');
		notes.add('third note');
		notes.add('fourth note');
		notes.add('fifth note');
	});
	it('should be able to delete the first index', function () {
		expect(notes.remove(0)).toBe(true);
		expect(notes.count()).toBe(4);
		console.log(notes.count());

	});

	it('should be able to delete the last index', function () {
		expect(notes.remove(-1)).toBe(true);
		expect(notes.count()).toBe(4);
		console.log(notes.count());

	});

	it('should return false if index is out of range', function () {
		expect(notes.remove(10)).toBe(false);

	});
	it('should return false if the parameter is missing', function () {
		expect(notes.remove()).toBe(false);
	});
});
describe('finding notes', function () {
	beforeEach(function () {
		notes.clear();
		notes.add('first note');
		notes.add('second note');
		notes.add('third note');
		notes.add('fourth note');
		notes.add('fifth note');

	});
	it('should be able to search without parameters', function () {
		notes.find();
		expect(notes.count()).toBe(5);
	});
	it('should be able to find specified strings', function () {
		notes.find('note');
		console.log(notes.find('note'));
		expect(notes.count()).toBe(5);
		pending();
	});


});