var notes = (function () {
	var list = [];

	return {

		add: function (note) {
			if (note && note !== ' ') {
				var item = {timestamp: Date.now(), text: note};
				list.push(item);
				return true;
			}
			return false;
		},


		remove: function (index) {
			if (index && index < list.length && index !== 0) {
				list.splice(index, 1);
				return true;
			} else if(index === 0) {
				list.shift();
				return true;
			}
			return false;
		},
		count: function () {
			return list.length;

		},
		list: function () {
		},
		find: function (str) {
			if (!str) {
				return list;
			}
			return false;
		},
		clear: function () {
			list.splice(0, list.length);
		},

	};
}());