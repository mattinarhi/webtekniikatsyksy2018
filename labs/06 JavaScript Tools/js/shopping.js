// shopping.js
// This script calculates an order total.

// Function called when the form is submitted.
// Function performs the calculation and returns false.
function calculate() {
	'use strict';
	
	// For storing the order total:
	var total;
    
	// Get references to the form values:
	var quantity = document.getElementById('quantity').value;
	var quantity1=parseInt(quantity);
	var price = document.getElementById('price').value;
	var price1= parseFloat(price);
	var tax = document.getElementById('tax').value;
	var tax1 = parseFloat(tax);
	var discount = document.getElementById('discount').value;
	var discount1 = parseFloat(discount);
	var shipping = document.getElementById('shipping').value;
	var shipping1 = parseFloat(shipping);



	// Add validation here later!
	
	// Calculate the initial total:
	total = quantity1 * price1;
	console.log('total before tax: ' + total);
	
	// Make the tax rate easier to use:
	tax1 = tax1 / 100;
	tax1 = tax1 + 1;
	
	// Factor in the tax:
	total = total * tax1;
	console.log('total after tax: ' + total);
		
	// Factor in the discount:
	if (quantity1>100) {
		total = total - 2 * discount1;
	}	else {
		total = total - discount1;
	}

	console.log('total after discount: ' + total);

	console.log('total before shipping: ' + total);

	total = total + shipping1;

	console.log('total after shipping: ' + total);

	// Format the total to two decimal places:
	total = total.toFixed(2);
	
	// Display the total:
	document.getElementById('total').value = total;
	
	// Return false to prevent submission:
	return false;
    
} // End of calculate() function.

// Function called when the window has been loaded.
// Function needs to add an event listener to the form.
function init() {
	'use strict';

	// Add an event listener to the form:
	var theForm = document.getElementById('theForm');
	theForm.onsubmit = calculate;

} // End of init() function.

// Assign an event listener to the window's load event:
window.onload = init;